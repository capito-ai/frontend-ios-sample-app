//
//  UIButton+Extensions.h
//  FastFooty
//
//  Created by Darren Harris on 10/28/13.
//  Copyright (c) 2013 Capito Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Extensions)

@property(nonatomic, assign) UIEdgeInsets hitTestEdgeInsets;

@end
