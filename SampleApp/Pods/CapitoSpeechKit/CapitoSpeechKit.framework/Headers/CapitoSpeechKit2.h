//
//  CapitoSpeechKit2.h
//  CapitoSpeechKit2
//
//  Created by Andrey Kozlov on 08/08/2017.
//  Copyright © 2017 Capito Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CapitoSpeechKit2.
FOUNDATION_EXPORT double CapitoSpeechKit2VersionNumber;

//! Project version string for CapitoSpeechKit2.
FOUNDATION_EXPORT const unsigned char CapitoSpeechKit2VersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CapitoSpeechKit2/PublicHeader.h>


