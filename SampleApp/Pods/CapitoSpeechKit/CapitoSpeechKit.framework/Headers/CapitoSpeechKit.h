//
//  CapitoSpeechKit.h
//  CapitoSpeechKit
//
//  Created by Andrey Kozlov on 08/08/2017.
//  Copyright © 2017 Capito Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CapitoSpeechKit.
FOUNDATION_EXPORT double CapitoSpeechKitVersionNumber;

//! Project version string for CapitoSpeechKit.
FOUNDATION_EXPORT const unsigned char CapitoSpeechKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CapitoSpeechKit/PublicHeader.h>
