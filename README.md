# README #

This is an iOS skeleton app to demonstrate how the Capito Natural Language Understanding Platform can be integrated in to iOS apps.

NOTE: Check the Xcode setting ENABLE_BITCODE in Build Settings of the project - it should be OFF. It may be ON by default, but our library is not supporting Bitcode at the moment.